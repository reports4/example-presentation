run:
	"/mnt/c/Program Files (x86)/Hamster Soft/Hamster PDF Reader/HamsterPDFReader.exe" main.pdf &
runl:
	xreader main.pdf &
clean:
	rm -f *.aux *.fdb_latexmk *.fls *.log *.out *.blg *.bbl *.ps *.synctex.gz *.tocs *.nav *.snm .toc
all: build run
	
build:
	latexmk -xelatex -synctex=1 main.tex
